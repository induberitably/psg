from flask_restful import Api, Resource, reqparse
from api.molder import Molder
import json 

#@app.route('/hello/<name>')
class MoldCountHandler(Resource):
    def get(self):
        molder = Molder()
        tab = json.dumps(molder.table)
        print(tab)
        return molder.table