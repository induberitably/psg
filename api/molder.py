from urllib.request import urlopen
from bs4 import BeautifulSoup
import locale
import json
class Molder():
    url = ""
    data = []
    table = {}
    def __init__(self):
        self.url = "https://www.southwestohioair.org/203/Pollen-Mold"
        page = ""
        # Connect to the website and return the html to the variable ‘page’
        try:
            page = urlopen(self.url)
        except:
            print("Error opening the URL")

        # parse the html using beautiful soup and store in variable `soup`
        soup = BeautifulSoup(page, 'html.parser')

        # Take out the <div> of name and get its value
        row_content = soup.find('table', {"class": "fr-alternate-rows"}).find_all('tr')

        article = ''
        self.data = []
        for row in row_content:
            cols = row.find_all('td')
            cols = [ele.text.strip() for ele in cols]
            self.data.append([ele for ele in cols if ele])
        
        d = {}
        for lists in self.data:
            if lists != []:
                d[lists[0]] = int(lists[1].replace(',', ''))#"yo"
        self.table = d
# m = Molder()
# print(m.data)