# save this as app.py
from flask import Flask, escape, request, render_template
#from flask import Flask, send_from_directory
from flask_restful import Api, Resource, reqparse
from flask_cors import CORS #comment this on deployment
#from api.TimeHandler import TimeHandler
from api.Hello import HelloApiHandler
from api.MoldCountHandler import MoldCountHandler

app = Flask(__name__, static_url_path='', static_folder='psgreact')
CORS(app) #comment this on deployment
api = Api(app)
@app.route("/", defaults={'path':''})
def serve(path):
    return send_from_directory(app.static_folder,'index.html')
api.add_resource(HelloApiHandler, '/hello/<string:name>')
api.add_resource(MoldCountHandler, '/mold/')
